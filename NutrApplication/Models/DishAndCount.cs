﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NutrApplication.Models
{
    class DishAndCount : INotifyPropertyChanged
    {

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
        private Dish _Dish;
        public Dish Dish
        {
            get { return _Dish; }
            set
            {
                if (_Dish != value)
                {
                    _Dish = value;
                    //try
                    //{
                    //    if (Directory.GetFiles(@"FoodPhotos\", Dish.Name + ".jpg").Length > 0)
                    //        FoodPhoto = new BitmapImage(new Uri(@"FoodPhotos\" + Dish.Name + ".jpg", UriKind.Relative));
                    //    else
                    //        FoodPhoto = null;
                    //}
                    //catch
                    //{
                    //    FoodPhoto = null;
                    //}
                    OnPropertyChanged("Dish");
                }
            }
        }
        private float _Count;
        public float Count
        {
            get { return _Count; }
            set
            {
                if (_Count != value)
                {
                    _Count = value;
                    OnPropertyChanged("Count");
                }
            }
        }

        public double Loc_Sum
        {
            get { return Count * (Dish.Price ?? 0) ; }

        }
        private string _IngredientList;
        public string IngredientList
        {
            get { return _IngredientList; }
            set
            {
                if (_IngredientList != value)
                {
                    _IngredientList = value;
                    OnPropertyChanged("IngredientList");
                }
            }
        }

        //private BitmapImage _FoodPhoto;
        public BitmapImage FoodPhoto
        {
            get {
                BitmapImage i;
                try
                {
                    if (Directory.GetFiles(@"FoodPhotos\", Dish.Name + ".jpg").Length > 0)
                        i = new BitmapImage(new Uri(@"FoodPhotos\" + Dish.Name + ".jpg", UriKind.Relative));
                    else
                        i = null;
                }
                catch
                {
                    i = null;
                }
                return i; }
            //set
            //{
            //    if (_FoodPhoto != value)
            //    {
            //        _FoodPhoto = value;
            //        OnPropertyChanged("FoodPhoto");
            //    }
            //}
        }

        public DishAndCount(Dish _dish, float _count, string _ingredientList)
        {
            this.Dish = _dish;
            this.Count = _count;
            this.IngredientList = _ingredientList;
        }
    }
}
