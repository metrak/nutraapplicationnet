//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NutrApplication.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TypeOfDish
    {
        public TypeOfDish()
        {
            this.Dishes = new HashSet<Dish>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<Dish> Dishes { get; set; }
    }
}
