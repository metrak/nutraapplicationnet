﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NutrApplication.Models
{

    class Table : INotifyPropertyChanged
    {

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
        public int index { get; set; }
        public string Name
        {
            get
            {
                if (index == -2)
                {
                    return "Сауна";
                }
                else if (index == -1)
                {
                    return @"Рома\Лена";
                }
                else
                    return "Стол " + index;
            }
        }

        private Boolean _AddBonusCardFlag;
        public Boolean AddBonusCardFlag
        {
            get { return _AddBonusCardFlag; }
            set
            {
                if (_AddBonusCardFlag != value)
                {
                    _AddBonusCardFlag = value;
                    if (_AddBonusCardFlag)
                    {
                        AddBonusCardVisibility = Visibility.Visible;
                    }
                    else
                    {
                        AddBonusCardVisibility = Visibility.Hidden;
                        SelectedBonusCard = null;
                    }
                    OnPropertyChanged("AddBonusCardFlag");
                }
            }
        }
        private Visibility _AddBonusCardVisibility;
        public Visibility AddBonusCardVisibility
        {
            get { return _AddBonusCardVisibility; }
            set
            {
                if (_AddBonusCardVisibility != value)
                {
                    _AddBonusCardVisibility = value;
                    OnPropertyChanged("AddBonusCardVisibility");
                }
            }
        }

        private ObservableCollection<BonusCard> _listBonusCards;
        public ObservableCollection<BonusCard> ListBonusCards
        {
            get { return _listBonusCards; }
            set
            {
                if (_listBonusCards != value)
                {
                    _listBonusCards = value;
                    OnPropertyChanged("ListBonusCards");
                }
            }
        }

        private BonusCard _selectedBonusCard;
        public BonusCard SelectedBonusCard
        {
            get { return _selectedBonusCard; }
            set
            {
                if (_selectedBonusCard != value)
                {
                    _selectedBonusCard = value;
                    AllPrice = AllPrice;
                    OnPropertyChanged("SelectedBonusCard");
                }
            }
        }

        public ObservableCollection<DishAndCount> _ListOfDis;
        public ObservableCollection<DishAndCount> ListOfDis
        {
            get { return _ListOfDis; }
            set
            {
                if (_ListOfDis != value)
                {
                    _ListOfDis = value;
                    OnPropertyChanged("ListOfDis");
                }
            }
        }
        private double _AllPrice;
        public double AllPrice
        {
            get { return _AllPrice; }
            set
            {
                _AllPrice = value;
                if (SelectedBonusCard != null)
                {
                    _SalePrice = _AllPrice * ((SelectedBonusCard.Bonus ?? 0) / 100);
                }
                else
                {
                    _SalePrice = 0;
                }
                if(_SalePrice != 0)
                    AllPriceWithSale = (_AllPrice - _SalePrice).ToString() + " (ск. " + _SalePrice.ToString() + ")";
                else
                    AllPriceWithSale = _AllPrice.ToString();
                OnPropertyChanged("AllPrice");
            }
        }

        public double _SalePrice;

        private string _AllPriceWithSale;
        public string AllPriceWithSale
        {
            get { return _AllPriceWithSale; }
            set
            {
                _AllPriceWithSale = value;
                OnPropertyChanged("AllPriceWithSale");
            }
        }

        public Table(int index)
        {
            this.index = index;
            this.ListOfDis = new ObservableCollection<DishAndCount>();
            AddBonusCardFlag = false;
            AddBonusCardVisibility = Visibility.Hidden;
            AllPrice = 0;
            using (var db = new BurDBEntities())
            {
                ListBonusCards = new ObservableCollection<BonusCard>(db.BonusCards.OrderBy(d => d.Name).ToList());
            }
        }
    }
}
