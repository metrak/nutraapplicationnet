﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NutrApplication.Models
{
    class TransactionInfo
    {
        public TransactionInfo(string n, string p, string a, string t, string u, string tm, string bonus)
        {
            Name = n;
            Price = p;
            Amount = a;
            Type = t;
            User = u;
            Time = tm;
            BonusCard = bonus;
        }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Amount { get; set; }
        public string Type { get; set; }
        public string User { get; set; }
        public string Time { get; set; }
        public double Summ
        {
            get
            {
                if (Price != "" && Amount != "")
                    return Convert.ToDouble(Price) * Convert.ToDouble(Amount);
                else
                    return 0;
            }
        }
        public string BonusCard { get; set; }
    }
}
