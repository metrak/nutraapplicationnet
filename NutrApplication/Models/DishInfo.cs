﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NutrApplication.Models
{
    class DishInfo : INotifyPropertyChanged
    {

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
        private Dish _Dish;
        public Dish Dish
        {
            get { return _Dish; }
            set
            {
                if (_Dish != value)
                {
                    _Dish = value;
                    OnPropertyChanged("Dish");
                }
            }
        }
        
        private string _IngredientList;
        public string IngredientList
        {
            get { return _IngredientList; }
            set
            {
                if (_IngredientList != value)
                {
                    _IngredientList = value;
                    OnPropertyChanged("IngredientList");
                }
            }
        }

        public BitmapImage FoodPhoto
        {
            get
            {
                BitmapImage i;
                try
                {
                    if (Directory.GetFiles(@"FoodPhotos\", Dish.Name + ".jpg").Length > 0)
                        i = new BitmapImage(new Uri(@"FoodPhotos\" + Dish.Name + ".jpg", UriKind.Relative));
                    else
                        i = null;
                }
                catch
                {
                    i = null;
                }
                return i;
            }
        }
        public DishInfo(Dish _dish, string _ingredientList)
        {
             
            this.Dish = _dish;
            this.IngredientList = _ingredientList;
            
        }
        
    }
}
