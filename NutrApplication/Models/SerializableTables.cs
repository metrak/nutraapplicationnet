﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NutrApplication.Models
{
    [Serializable]
    public class SerializableTables
    {
        [Serializable]
        public class strDish
        {
            public int idDish;
            public float countDish;
        }
        public int index { get; set; }

        public List<strDish> ListOfDish { get; set; }
        //public double AllPrice { get; set; }

        public SerializableTables(int index)
        {
            this.index = index;
            this.ListOfDish = new List<strDish>();
            //AllPrice = 0;
        }

        public void addDish(int idD, float countD)
        {
            strDish sD = ListOfDish.FirstOrDefault(o => o.idDish == idD);
            if (sD != null)
                ListOfDish.Remove(sD);
            ListOfDish.Add(new strDish() { idDish = idD, countDish = countD });
        }
        public void removeDish(int idD)
        {
            strDish sD = ListOfDish.FirstOrDefault(o => o.idDish == idD);
            if (sD != null)
                ListOfDish.Remove(sD);
        }
    }
}
