﻿#pragma checksum "..\..\..\Views\AdminView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "5878E3C847F2CCE4BA92A7BB65EC0BB1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using NutrApplication.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace NutrApplication.Views {
    
    
    /// <summary>
    /// AdminView
    /// </summary>
    public partial class AdminView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dummygrid;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummywidth1;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummywidth2;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummywidth3;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummywidth4;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummywidth5;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox PasswordUserBox;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dummy2grid;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy2width1;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy2width2;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy2width3;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy2width4;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy2width5;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy2width6;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dummy3grid;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy3width1;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy3width2;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy3width3;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy3width4;
        
        #line default
        #line hidden
        
        
        #line 229 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dummy4grid;
        
        #line default
        #line hidden
        
        
        #line 235 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy4width1;
        
        #line default
        #line hidden
        
        
        #line 236 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy4width2;
        
        #line default
        #line hidden
        
        
        #line 237 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy4width3;
        
        #line default
        #line hidden
        
        
        #line 272 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dummy5grid;
        
        #line default
        #line hidden
        
        
        #line 283 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width1;
        
        #line default
        #line hidden
        
        
        #line 284 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width2;
        
        #line default
        #line hidden
        
        
        #line 285 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width3;
        
        #line default
        #line hidden
        
        
        #line 286 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width4;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width5;
        
        #line default
        #line hidden
        
        
        #line 288 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width6;
        
        #line default
        #line hidden
        
        
        #line 289 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width7;
        
        #line default
        #line hidden
        
        
        #line 290 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy5width8;
        
        #line default
        #line hidden
        
        
        #line 363 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dummy8grid;
        
        #line default
        #line hidden
        
        
        #line 374 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width1;
        
        #line default
        #line hidden
        
        
        #line 375 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width2;
        
        #line default
        #line hidden
        
        
        #line 376 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width3;
        
        #line default
        #line hidden
        
        
        #line 377 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width4;
        
        #line default
        #line hidden
        
        
        #line 378 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width5;
        
        #line default
        #line hidden
        
        
        #line 379 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width6;
        
        #line default
        #line hidden
        
        
        #line 380 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width7;
        
        #line default
        #line hidden
        
        
        #line 381 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy8width8;
        
        #line default
        #line hidden
        
        
        #line 440 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dummy6grid;
        
        #line default
        #line hidden
        
        
        #line 447 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy6width1;
        
        #line default
        #line hidden
        
        
        #line 448 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy6width2;
        
        #line default
        #line hidden
        
        
        #line 449 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy6width3;
        
        #line default
        #line hidden
        
        
        #line 450 "..\..\..\Views\AdminView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dummy6width4;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/NutrApplication;component/views/adminview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\AdminView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dummygrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.dummywidth1 = ((System.Windows.Controls.Border)(target));
            return;
            case 3:
            this.dummywidth2 = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.dummywidth3 = ((System.Windows.Controls.Border)(target));
            return;
            case 5:
            this.dummywidth4 = ((System.Windows.Controls.Border)(target));
            return;
            case 6:
            this.dummywidth5 = ((System.Windows.Controls.Border)(target));
            return;
            case 7:
            this.PasswordUserBox = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 8:
            this.dummy2grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 9:
            this.dummy2width1 = ((System.Windows.Controls.Border)(target));
            return;
            case 10:
            this.dummy2width2 = ((System.Windows.Controls.Border)(target));
            return;
            case 11:
            this.dummy2width3 = ((System.Windows.Controls.Border)(target));
            return;
            case 12:
            this.dummy2width4 = ((System.Windows.Controls.Border)(target));
            return;
            case 13:
            this.dummy2width5 = ((System.Windows.Controls.Border)(target));
            return;
            case 14:
            this.dummy2width6 = ((System.Windows.Controls.Border)(target));
            return;
            case 15:
            this.dummy3grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.dummy3width1 = ((System.Windows.Controls.Border)(target));
            return;
            case 17:
            this.dummy3width2 = ((System.Windows.Controls.Border)(target));
            return;
            case 18:
            this.dummy3width3 = ((System.Windows.Controls.Border)(target));
            return;
            case 19:
            this.dummy3width4 = ((System.Windows.Controls.Border)(target));
            return;
            case 20:
            this.dummy4grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 21:
            this.dummy4width1 = ((System.Windows.Controls.Border)(target));
            return;
            case 22:
            this.dummy4width2 = ((System.Windows.Controls.Border)(target));
            return;
            case 23:
            this.dummy4width3 = ((System.Windows.Controls.Border)(target));
            return;
            case 24:
            this.dummy5grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 25:
            this.dummy5width1 = ((System.Windows.Controls.Border)(target));
            return;
            case 26:
            this.dummy5width2 = ((System.Windows.Controls.Border)(target));
            return;
            case 27:
            this.dummy5width3 = ((System.Windows.Controls.Border)(target));
            return;
            case 28:
            this.dummy5width4 = ((System.Windows.Controls.Border)(target));
            return;
            case 29:
            this.dummy5width5 = ((System.Windows.Controls.Border)(target));
            return;
            case 30:
            this.dummy5width6 = ((System.Windows.Controls.Border)(target));
            return;
            case 31:
            this.dummy5width7 = ((System.Windows.Controls.Border)(target));
            return;
            case 32:
            this.dummy5width8 = ((System.Windows.Controls.Border)(target));
            return;
            case 33:
            this.dummy8grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 34:
            this.dummy8width1 = ((System.Windows.Controls.Border)(target));
            return;
            case 35:
            this.dummy8width2 = ((System.Windows.Controls.Border)(target));
            return;
            case 36:
            this.dummy8width3 = ((System.Windows.Controls.Border)(target));
            return;
            case 37:
            this.dummy8width4 = ((System.Windows.Controls.Border)(target));
            return;
            case 38:
            this.dummy8width5 = ((System.Windows.Controls.Border)(target));
            return;
            case 39:
            this.dummy8width6 = ((System.Windows.Controls.Border)(target));
            return;
            case 40:
            this.dummy8width7 = ((System.Windows.Controls.Border)(target));
            return;
            case 41:
            this.dummy8width8 = ((System.Windows.Controls.Border)(target));
            return;
            case 42:
            this.dummy6grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 43:
            this.dummy6width1 = ((System.Windows.Controls.Border)(target));
            return;
            case 44:
            this.dummy6width2 = ((System.Windows.Controls.Border)(target));
            return;
            case 45:
            this.dummy6width3 = ((System.Windows.Controls.Border)(target));
            return;
            case 46:
            this.dummy6width4 = ((System.Windows.Controls.Border)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

