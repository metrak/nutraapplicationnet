﻿using NutrApplication.ViewModels;
using NutrApplication.Views;
using System.Windows;

namespace NutrApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var mw = new MainWindow
            {
                DataContext = new MainWindowViewModel()
            };

            mw.Show();
        }
    }
}
