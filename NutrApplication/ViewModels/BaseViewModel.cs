﻿using NutrApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NutrApplication.ViewModels
{
    abstract class BaseViewModel
    {
        public ICommand ExitCommand { get; set; }

        protected BaseViewModel()
        {
            ExitCommand = new Command(arg => ViewShower.Exit());
        }
        public User user { get; set; }
    }
}
