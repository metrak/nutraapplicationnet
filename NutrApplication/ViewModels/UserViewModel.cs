﻿using NutrApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace NutrApplication.ViewModels
{
    class UserViewModel : BaseViewModel, INotifyPropertyChanged
    {

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Constructor
        public UserViewModel()
        {
            Tables = new ObservableCollection<Table>();
            SerTables = new List<SerializableTables>();

            int CountTables = 6;

            Tables.Add(new Table(-1));
            Tables.Add(new Table(-2));
            SerTables.Add(new SerializableTables(-1));
            SerTables.Add(new SerializableTables(-2));
            for (int i = 0; i < CountTables; i++)
            {
                Tables.Add(new Table(i + 1));
                SerTables.Add(new SerializableTables(i + 1));
            }

            CurrentTable = Tables[0];
            AllTransaction = 0;
            LoadData();
            NewDish = new DishAndCount(null, 1, "");
            QuikNewDish = new DishAndCount(null, 1, "");
            NewTransaction = new Transaction();

            AddTableCommand = new Command(arg =>
            {
                Tables.Add(new Table(Tables.Last().index + 1));
                SerTables.Add(new SerializableTables(SerTables.Last().index + 1));
            });
            DelTableCommand = new Command(arg => DelTableMethod());
            DelDishCommand = new Command(arg => DelDishMethod(arg));
            AddDishCommand = new Command(arg => AddDishMethod());
            CloseBillCommand = new Command(arg => CloseBillMethod());
            PrintBillCommand = new Command(arg => PrintBillMethod());
            QuikCloseBillCommand = new Command(arg => QuikCloseBillMethod());
            AddBuyCommand = new Command(arg => AddBuyMethod());
            DeSerializableCommand = new Command(arg => DeSerializableMethod());

            AddToStorageFlag = false;
            AddToStorageVisibility = Visibility.Hidden;
            AddToStorageVisibility_NOT = Visibility.Visible;

            

            CloseCass = DateTime.Today;
            CloseCass = CloseCass.AddHours(22);
            CloseCass = CloseCass.AddMinutes(30);

            OpenCass = DateTime.Today;
            OpenCass = OpenCass.AddHours(16);
        }
        #endregion

        #region Properties
        DateTime CloseCass;
        DateTime OpenCass;
        public ObservableCollection<Table> Tables { get; set; }
        public List<SerializableTables> SerTables { get; set; }
        public ObservableCollection<TransactionInfo> ListOfTransaction { get; set; }

        private Table _currentTable;
        public Table CurrentTable
        {
            get { return _currentTable; }
            set
            {
                if (_currentTable != value)
                {
                    _currentTable = value;
                    OnPropertyChanged("CurrentTable");
                }
            }
        }
        
        private DishAndCount _selectedDish;
        public DishAndCount SelectedDish
        {
            get { return _selectedDish; }
            set
            {
                if (_selectedDish != value)
                {
                    _selectedDish = value;
                    OnPropertyChanged("SelectedDish");
                }
            }
        }
        
        private string _AddIngredientToStore_BAR_Measurement_Name;
        public string AddIngredientToStore_BAR_Measurement_Name
        {
            get { return _AddIngredientToStore_BAR_Measurement_Name; }
            set
            {
                if (_AddIngredientToStore_BAR_Measurement_Name != value)
                {
                    _AddIngredientToStore_BAR_Measurement_Name = value;
                    OnPropertyChanged("AddIngredientToStore_BAR_Measurement_Name");
                }
            }
        }
        private Boolean _AddToStorageFlag;
        public Boolean AddToStorageFlag
        {
            get { return _AddToStorageFlag; }
            set
            {
                if (_AddToStorageFlag != value)
                {
                    _AddToStorageFlag = value;
                    if(_AddToStorageFlag)
                    {
                        AddToStorageVisibility = Visibility.Visible;
                        AddToStorageVisibility_NOT = Visibility.Hidden;
                    }
                    else
                    {
                        AddToStorageVisibility = Visibility.Hidden;
                        AddToStorageVisibility_NOT = Visibility.Visible;
                    }
                    OnPropertyChanged("AddToStorageFlag");
                }
            }
        }
        private Visibility _AddToStorageVisibility;
        public Visibility AddToStorageVisibility
        {
            get { return _AddToStorageVisibility; }
            set
            {
                if (_AddToStorageVisibility != value)
                {
                    _AddToStorageVisibility = value;
                    OnPropertyChanged("AddToStorageVisibility");
                }
            }
        }

        
        private Visibility _AddToStorageVisibility_NOT;
        public Visibility AddToStorageVisibility_NOT
        {
            get { return _AddToStorageVisibility_NOT; }
            set
            {
                if (_AddToStorageVisibility_NOT != value)
                {
                    _AddToStorageVisibility_NOT = value;
                    OnPropertyChanged("AddToStorageVisibility_NOT");
                }
            }
        }
        private Ingredient _addIngredientToStore_BAR;
        public Ingredient AddIngredientToStore_BAR
        {
            get { return _addIngredientToStore_BAR; }
            set
            {
                if (_addIngredientToStore_BAR != value)
                {
                    if (value != null)
                    {
                        _addIngredientToStore_BAR = new Ingredient() { Id = value.Id, Name = value.Name };
                        AddIngredientToStore_BAR_Measurement_Name = value.Measurement.Name;
                    }
                    else
                        _addIngredientToStore_BAR = value;
                    OnPropertyChanged("AddIngredientToStore_BAR");
                }
            }
        }



        private DishAndCount _newDish;
        public DishAndCount NewDish
        {
            get { return _newDish; }
            set
            {
                if (_newDish != value)
                {
                    _newDish = value;
                    OnPropertyChanged("NewDish");
                }
            }
        }

        private DishAndCount _quikNewDish;
        public DishAndCount QuikNewDish
        {
            get { return _quikNewDish; }
            set
            {
                if (_quikNewDish != value)
                {
                    _quikNewDish = value;
                    OnPropertyChanged("QuikNewDish");
                }
            }
        }

        private Transaction _newTransaction;
        public Transaction NewTransaction
        {
            get { return _newTransaction; }
            set
            {
                if (_newTransaction != value)
                {
                    _newTransaction = value;
                    OnPropertyChanged("NewTransaction");
                }
            }
        }

        private double _AllTransaction;
        public double AllTransaction
        {
            get { return _AllTransaction; }
            set
            {
                if (_AllTransaction != value)
                {
                    _AllTransaction = value;
                    OnPropertyChanged("AllTransaction");
                }
            }
        }
        public ObservableCollection<Dish> ListOfDish { get; set; }

        private ObservableCollection<Ingredient> _listIngredients;
        public ObservableCollection<Ingredient> ListIngredients
        {
            get { return _listIngredients; }
            set
            {
                if (_listIngredients != value)
                {
                    _listIngredients = value;
                    OnPropertyChanged("ListIngredients");
                }
            }
        }

        

        #endregion

        #region Commands
        public ICommand AddTableCommand { get; set; }
        public ICommand DelTableCommand { get; set; }
        public ICommand DelDishCommand { get; set; }
        public ICommand AddDishCommand { get; set; }
        public ICommand CloseBillCommand { get; set; }
        public ICommand PrintBillCommand { get; set; }
        public ICommand QuikCloseBillCommand { get; set; }
        public ICommand AddBuyCommand { get; set; }
        public ICommand DeSerializableCommand { get; set; }
        #endregion

        #region Methods
        void DelTableMethod()
        {
            if (CurrentTable != null)
                if (CurrentTable.ListOfDis.Count == 0)
                {
                    SerTables.Remove(SerTables.FirstOrDefault(t => t.index == CurrentTable.index));
                    Tables.Remove(CurrentTable);
                }
                else
                    MessageBox.Show("Перед удалением закройте счет!");
        }
        void DelDishMethod(object dish)
        {
            SelectedDish = CurrentTable.ListOfDis.FirstOrDefault(d => d.Dish.Id == ((Dish)dish).Id);
            CurrentTable.AllPrice -= SelectedDish.Count * SelectedDish.Dish.Price ?? 0;
            //CurrentTable.ListOfDis.Remove(SelectedDish);

            SerTables.FirstOrDefault(t => t.index == CurrentTable.index).removeDish(SelectedDish.Dish.Id);

            CurrentTable.ListOfDis.Remove(SelectedDish);

            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream("tables.dat", FileMode.OpenOrCreate))
            {
                // сериализуем весь массив people
                formatter.Serialize(fs, SerTables);
            }
        }
        void AddDishMethod()
        {
            if (DateTime.Now > OpenCass)
            {
                MessageBox.Show("\nОткрой кассу! Штраф 200 грн.!!!\n");
                OpenCass = OpenCass.AddDays(1);
            }
            if (DateTime.Now > CloseCass)
            {
                MessageBox.Show("\nЗакрой кассу! Штраф 200 грн.!!!\n");
                CloseCass = CloseCass.AddDays(1);
            }
            if (NewDish.Dish != null && CurrentTable != null)
            {
                string errors = "";
                string needBuy = "";
                DishAndCount oldDish = null;
                float inOrdersCount = NewDish.Count;
                string ingredientList = "";
                foreach (Table t in Tables)
                {
                    oldDish = t.ListOfDis.FirstOrDefault(d => d.Dish.Id == NewDish.Dish.Id);
                    if (oldDish != null)
                        inOrdersCount += oldDish.Count;
                }
                using (var db = new BurDBEntities())
                {
                    ingredientList = "";
                    foreach (DishIngredientList ingred in db.DishIngredientLists.Where(i => i.IdDish == NewDish.Dish.Id))
                    {
                        if (ingred.Amount * inOrdersCount > ingred.Ingredient.Amount)
                        {
                            errors += "Недостаточно \"" + ingred.Ingredient.Name + "\" на складе. " + "Осталось: " + ingred.Ingredient.Amount + ", нужно: " + ingred.Amount * inOrdersCount + "\n";
                            needBuy += "\t" + ingred.Ingredient.Name + "\n";
                        }

                        ingredientList += string.Format("\n{0,-20} {1}", ingred.Ingredient.Name, ingred.Amount + ingred.Ingredient.Measurement.Name);
                        
                    }
                    if (ingredientList != "")
                        ingredientList = ingredientList.Remove(0, 1);
                }
                if (errors.Length == 0)
                {
                    oldDish = CurrentTable.ListOfDis.FirstOrDefault(d => d.Dish.Id == NewDish.Dish.Id);
                    if (oldDish != null)
                    {
                        CurrentTable.AllPrice -= oldDish.Count * oldDish.Dish.Price ?? 0;
                        CurrentTable.ListOfDis.Remove(oldDish);
                        NewDish.Count += oldDish.Count;
                    }
                    CurrentTable.ListOfDis.Add(new DishAndCount(NewDish.Dish, NewDish.Count, ingredientList));
                    CurrentTable.AllPrice += NewDish.Count * NewDish.Dish.Price ?? 0;
                    
                    SerTables.FirstOrDefault(t => t.index == CurrentTable.index).addDish(NewDish.Dish.Id, NewDish.Count);

                    NewDish.Dish = null;
                    NewDish.Count = 1;

                    BinaryFormatter formatter = new BinaryFormatter();
                    using (FileStream fs = new FileStream("tables.dat", FileMode.OpenOrCreate))
                    {
                        // сериализуем весь массив people
                        formatter.Serialize(fs, SerTables);
                    }
                }
                else
                    MessageBox.Show(errors.ToString() + "\nНужно купить:\n" + needBuy);
            }
        }
        void LoadData()
        {
            using (var db = new BurDBEntities())
            {
                string nameUser;
                ListOfDish = new ObservableCollection<Dish>(db.Dishes.OrderBy(d => d.Name).ToList());
                DateTime date = DateTime.Now;
                if (date.Hour < 9)
                    date = date.AddDays(-1);
                DateTime StartTime = new DateTime(date.Year, date.Month, date.Day, 9, 0, 0);
                DateTime EndTime = StartTime.AddDays(1);
                ListOfTransaction = new ObservableCollection<TransactionInfo>();
                foreach (var tran in db.Transactions.Where(t => t.Date > StartTime && t.Date < EndTime).OrderBy(t => t.Date).ToList())
                {
                    if (tran.IdUser == null) nameUser = "<удален>"; else nameUser = tran.User.FullName;
                    ListOfTransaction.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), tran.TypeOfTransaction.Name, nameUser, tran.Date.Value.ToShortTimeString(), ""));
                    AllTransaction += (tran.Amount ?? 0) * (tran.Price ?? 0);
                }
            }
            using (var db = new BurDBEntities())
            {
                ObservableCollection<Measurement>  ListMeasurements = new ObservableCollection<Measurement>(db.Measurements.OrderBy(m => m.Name));
                ListIngredients = new ObservableCollection<Ingredient>(db.Ingredients.OrderBy(i => i.Name));
                foreach (Ingredient i_ingredient in ListIngredients)
                    i_ingredient.Measurement = ListMeasurements.FirstOrDefault(m => m.Id == i_ingredient.IdMeasurement);
            }
        }

        bool CheakUseBonusToday()
        {
            if (CurrentTable.SelectedBonusCard != null)
            {
                DateTime TimeOfTransaction = DateTime.Now.AddDays(-1);
                using (var db = new BurDBEntities())
                {
                    if (db.Transactions.FirstOrDefault(t => t.Date > TimeOfTransaction && t.BonusCardNumbet == CurrentTable.SelectedBonusCard.Card_number) != null)
                        return false;
                }
            }
            return true;
        }
        void CloseBillMethod()
        {
            if (CheakUseBonusToday())
            {
                MessageBoxResult result = MessageBox.Show("Вы уверены?", "Confirmation", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    PrintBillMethod();
                    using (var db = new BurDBEntities())
                    {
                        foreach (DishAndCount dish in CurrentTable.ListOfDis)
                        {
                            var tran = new Transaction()
                            {
                                Name = dish.Dish.Name,
                                Date = DateTime.Now,
                                Amount = dish.Count,
                                IdType = 1,
                                Price = dish.Dish.Price,
                                IdUser = user.Id
                            };
                            if (CurrentTable.SelectedBonusCard != null)
                            {
                                tran.BonusName = CurrentTable.SelectedBonusCard.Name;
                                tran.BonusCardNumbet = CurrentTable.SelectedBonusCard.Card_number;
                                tran.Bonus = (CurrentTable.SelectedBonusCard.Bonus ?? 0).ToString() + "%";
                                tran.Price = tran.Price * (1 - ((CurrentTable.SelectedBonusCard.Bonus ?? 0) / 100));
                            }
                            ListOfTransaction.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), "sell", user.FullName, tran.Date.Value.ToShortTimeString(), ""));
                            AllTransaction += (tran.Amount ?? 0) * (tran.Price ?? 0);
                            db.Transactions.Add(tran);
                            foreach (DishIngredientList ingred in db.DishIngredientLists.Where(i => i.IdDish == dish.Dish.Id))
                            {
                                double amount = ingred.Amount ?? 0;
                                var updIngred = db.Ingredients.Where(i => i.Id == ingred.IdIngredient).First();
                                updIngred.Amount -= amount * dish.Count;
                            }
                        }

                        SerTables.FirstOrDefault(t => t.index == CurrentTable.index).ListOfDish = new List<SerializableTables.strDish>();
                        BinaryFormatter formatter = new BinaryFormatter();
                        using (FileStream fs = new FileStream("tables.dat", FileMode.OpenOrCreate))
                        {
                            // сериализуем весь массив people
                            formatter.Serialize(fs, SerTables);
                        }

                        CurrentTable.ListOfDis = new ObservableCollection<DishAndCount>();
                        CurrentTable.AllPrice = 0;
                        CurrentTable.AddBonusCardFlag = false;
                        CurrentTable.SelectedBonusCard = null;
                        db.SaveChanges();
                        ObservableCollection<Measurement> ListMeasurements = new ObservableCollection<Measurement>(db.Measurements.OrderBy(m => m.Name));
                        ListIngredients = new ObservableCollection<Ingredient>(db.Ingredients.OrderBy(i => i.Name));
                        foreach (Ingredient i_ingredient in ListIngredients)
                            i_ingredient.Measurement = ListMeasurements.FirstOrDefault(m => m.Id == i_ingredient.IdMeasurement);

                    }
                }
            }
            else
            {
                MessageBox.Show("Эта бонусная карта использовалась менее 24 часов назад!!!", "Confirmation");
            }
        }
        
        private Font printFont;
        private StreamReader streamToPrint;
        void PrintBillMethod()
        {
            StreamWriter file = new StreamWriter("print.txt");
            string strInBill1, strInBill2;
            strInBill1 = "\"НУТРЬ\"";
            strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
            file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            strInBill1 = "ФОП\"Крижанівська О.В.\"";
            strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
            file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            strInBill1 = "м.Київ,бул.А.Вернадського,79";
            strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
            file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            strInBill1 = "ІД  3344410140";
            strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
            file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            strInBill1 = "ЗН АТ401300221  ФН 3000232091";
            strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
            file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            strInBill1 = "Оператор30 Каса 01";
            strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
            file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            file.WriteLine(new string('-', 30));
            foreach (DishAndCount dish in CurrentTable.ListOfDis)
            {
                strInBill1 = dish.Dish.Name;
                if (strInBill1.Length > 30)
                    strInBill1 = dish.Dish.Name.Substring(0, 30);
                file.WriteLine(strInBill1);
                strInBill1 = dish.Count + " x " + dish.Dish.Price + " = ";
                strInBill2 = dish.Count * dish.Dish.Price + " грн.";
                strInBill1 = strInBill1 + new string(' ', 30 - strInBill1.Length - strInBill2.Length) + strInBill2;
                file.WriteLine(strInBill1);
            }

            if (CurrentTable.SelectedBonusCard != null)
            {
                strInBill1 = "Скидка ";
                strInBill2 = (-CurrentTable._SalePrice).ToString() + " грн.";
                strInBill1 = strInBill1 + new string(' ', 30 - strInBill1.Length - strInBill2.Length) + strInBill2;
                file.WriteLine(strInBill1);

                file.WriteLine(new string('-', 30));
                strInBill1 = "Сумма к оплате ";
                strInBill2 = (CurrentTable.AllPrice - CurrentTable._SalePrice).ToString() + " грн.";
                strInBill1 = strInBill1 + new string(' ', 30 - strInBill1.Length - strInBill2.Length) + strInBill2;
                file.WriteLine(strInBill1);
            }
            else
            {

                file.WriteLine(new string('-', 30));
                strInBill1 = "Сумма к оплате ";
                strInBill2 = CurrentTable.AllPrice.ToString() + " грн.";
                strInBill1 = strInBill1 + new string(' ', 30 - strInBill1.Length - strInBill2.Length) + strInBill2;
                file.WriteLine(strInBill1);
            }

            

            file.WriteLine();
            strInBill1 = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
            file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            if (CurrentTable.SelectedBonusCard != null)
            {
                strInBill1 = "Благодарим Вас,";
                strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
                file.WriteLine(strInBill2 + strInBill1 + strInBill2);

                strInBill1 = CurrentTable.SelectedBonusCard.Name;
                strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
                file.WriteLine(strInBill2 + strInBill1 + strInBill2);

                strInBill1 = "за выбор нашего заведения!";
                strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
                file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            }
            else
            {
                strInBill1 = "Спасибо:) Всегда рады Вам!!!";
                strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
                file.WriteLine(strInBill2 + strInBill1 + strInBill2);
            }

            using (var db = new BurDBEntities())
            {
                int countTexts = db.TextInSells.Count(t => t.Flag != 0);
                if (countTexts > 0)
                {
                    file.WriteLine();
                    Random random = new Random();
                    int rndText = random.Next(countTexts);
                    List<string> listTexts = db.TextInSells.Where(t => t.Flag != 0).Select(t => t.Text).ToList();
                    string[] listSplitTexts = listTexts[rndText].Split('\\');
                    foreach (string str in listSplitTexts)
                    {
                        strInBill1 = str;
                        strInBill2 = new string(' ', (30 - strInBill1.Length) / 2);
                        file.WriteLine(strInBill2 + strInBill1 + strInBill2);
                    }
                }
            }

            file.Close();
            try
            {
                streamToPrint = new StreamReader
                   ("print.txt");
                try
                {
                    printFont = new Font("Lucida console", 10);
                    PrintDocument pd = new PrintDocument();
                    pd.PrintPage += new PrintPageEventHandler
                       (this.pd_PrintPage);
                    pd.Print();
                }
                finally
                {
                    streamToPrint.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 2;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

            // Calculate the number of lines per page..
            linesPerPage = 29;
            //linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }

        bool QuikAddDishMethod()
        {
            string errors = "";
            using (var db = new BurDBEntities())
            {
                foreach (DishIngredientList ingred in db.DishIngredientLists.Where(i => i.IdDish == QuikNewDish.Dish.Id))
                {
                    if (ingred.Amount * QuikNewDish.Count > ingred.Ingredient.Amount)
                        errors += "Недостаточно \"" + ingred.Ingredient.Name + "\" на складе. " + "Осталось: " + ingred.Ingredient.Amount + ", нужно: " + ingred.Amount * QuikNewDish.Count + "\n";
                }
            }
            if (errors.Length == 0)
                return true;
            else
            {
                MessageBox.Show(errors.ToString());
                return false;
            }
        }
        void QuikCloseBillMethod()
        {
            MessageBoxResult result = MessageBox.Show("Вы уверены?", "Confirmation", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes && QuikNewDish.Dish != null && QuikAddDishMethod())
            {
                using (var db = new BurDBEntities())
                {
                    var tran = new Transaction()
                    {
                        Name = QuikNewDish.Dish.Name,
                        Date = DateTime.Now,
                        Amount = QuikNewDish.Count,
                        IdType = 1,
                        Price = QuikNewDish.Dish.Price,
                        IdUser = user.Id
                    };
                    ListOfTransaction.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), "sell", user.FullName, tran.Date.Value.ToShortTimeString(), ""));
                    AllTransaction += (tran.Amount ?? 0) * (tran.Price ?? 0);
                    db.Transactions.Add(tran);
                    foreach (DishIngredientList ingred in db.DishIngredientLists.Where(i => i.IdDish == QuikNewDish.Dish.Id))
                    {
                        double amount = ingred.Amount ?? 0;
                        var updIngred = db.Ingredients.Where(i => i.Id == ingred.IdIngredient).First();
                        updIngred.Amount -= amount * QuikNewDish.Count;
                    }
                    db.SaveChanges();
                    QuikNewDish = new DishAndCount(null, 1, "");
                    ObservableCollection<Measurement> ListMeasurements = new ObservableCollection<Measurement>(db.Measurements.OrderBy(m => m.Name));
                    ListIngredients = new ObservableCollection<Ingredient>(db.Ingredients.OrderBy(i => i.Name));
                    foreach (Ingredient i_ingredient in ListIngredients)
                        i_ingredient.Measurement = ListMeasurements.FirstOrDefault(m => m.Id == i_ingredient.IdMeasurement);

                }
            }
        }
        void AddBuyMethod()
        {
            if (AddToStorageFlag)
            {
                if (AddIngredientToStore_BAR != null && NewTransaction.Price.HasValue && NewTransaction.Amount.HasValue && NewTransaction.Amount.Value > 0)
                {
                    using (var db = new BurDBEntities())
                    {
                        db.Ingredients.FirstOrDefault(i => i.Id == AddIngredientToStore_BAR.Id).Amount += NewTransaction.Amount.Value;

                        var tran = new Transaction()
                        {
                            Name = AddIngredientToStore_BAR.Name,
                            Date = DateTime.Now,
                            Amount = NewTransaction.Amount,
                            IdType = 2,
                            Price = -(NewTransaction.Price / NewTransaction.Amount),
                            IdUser = user.Id
                        };
                        ListOfTransaction.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), "buy", user.FullName, tran.Date.Value.ToShortTimeString(), ""));
                        AllTransaction += (tran.Amount ?? 0) * (tran.Price ?? 0);
                        db.Transactions.Add(tran);


                        db.SaveChanges();
                    }
                    ListIngredients.FirstOrDefault(i => i.Id == AddIngredientToStore_BAR.Id).Amount += NewTransaction.Amount.Value;
                    ListIngredients = new ObservableCollection<Ingredient>(ListIngredients.OrderBy(i => i.Name));
                    AddIngredientToStore_BAR = null;
                    
                    NewTransaction = new Transaction();
                }
                else MessageBox.Show("Заполните все поля!");
            }
            else
            {
                if (NewTransaction.Name != "" && NewTransaction.Price.HasValue && NewTransaction.Amount.HasValue)
                {
                    using (var db = new BurDBEntities())
                    {
                        var tran = new Transaction()
                        {
                            Name = NewTransaction.Name,
                            Date = DateTime.Now,
                            Amount = NewTransaction.Amount,
                            IdType = 2,
                            Price = -(NewTransaction.Price/NewTransaction.Amount),
                            IdUser = user.Id
                        };
                        ListOfTransaction.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), "buy", user.FullName, tran.Date.Value.ToShortTimeString(), ""));
                        AllTransaction += (tran.Amount ?? 0) * (tran.Price ?? 0);
                        db.Transactions.Add(tran);
                        db.SaveChanges();

                        // не знаю как это тут появилось
                        //var Bot = new Telegram.Bot.TelegramBotClient("340238083:AAHitTGFol3oJyMxOvBxBUuUh1pMtAvjQoI");
                        //var t = Bot.SendTextMessageAsync("@nutr_Bar", "Заказ на стол ");
                        //t.Wait();
                    }
                    NewTransaction = new Transaction();
                }
                else MessageBox.Show("Заполните все поля!");
            }
        }
        void DeSerializableMethod()
        {
            
            List<SerializableTables> DeSerTables;
            BinaryFormatter formatter = new BinaryFormatter();
            // десериализация
            using (FileStream fs = new FileStream("tables.dat", FileMode.OpenOrCreate))
            {
                DeSerTables = (List<SerializableTables>)formatter.Deserialize(fs);
            }
            foreach(SerializableTables deserTable in DeSerTables)
            {
                CurrentTable = Tables.FirstOrDefault(t => t.index == deserTable.index);
                if(CurrentTable==null)
                {
                    Tables.Add(new Table(deserTable.index));
                    SerTables.Add(new SerializableTables(deserTable.index));
                }
                CurrentTable = Tables.FirstOrDefault(t => t.index == deserTable.index);
                CurrentTable.ListOfDis = new ObservableCollection<DishAndCount>();
                CurrentTable.AllPrice = 0;
                foreach (var deserDish in deserTable.ListOfDish)
                {
                    NewDish.Dish = ListOfDish.FirstOrDefault(d => d.Id == deserDish.idDish);
                    NewDish.Count = deserDish.countDish;
                    AddDishMethod();
                }
            }
            CurrentTable = Tables.First();
        }
        #endregion

    }
}
