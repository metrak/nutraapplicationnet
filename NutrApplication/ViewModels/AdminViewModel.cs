﻿using Microsoft.Win32;
using NutrApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Drawing;

namespace NutrApplication.ViewModels
{
    class AdminViewModel : BaseViewModel, INotifyPropertyChanged
    {

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Constructor
        public AdminViewModel()
        {
            //AddBuyCommand = new Command(arg => AddBuyMethod());
            SelectedUser = new User();
            FlagSell = true; 
            FlagBuy = true;
            FlagSellCommon = true;
            FlagBuyCommon = true;
            LoadUserData();

            SaveUserCommand = new Command((pass) => SaveUserMethod(pass));
            NewUserCommand = new Command((pass) => NewUserMethod(pass));
            DeleteUserCommand = new Command(arg => DeleteUserMethod(arg));

            SaveDishCommand = new Command(arg => SaveDishMethod());
            NewDishCommand = new Command(arg => NewDishMethod());
            DeleteDishCommand = new Command(arg => DeleteDishMethod(arg));

            SaveIngredientCommand = new Command(arg => SaveIngredientMethod());
            NewIngredientCommand = new Command(arg => NewIngredientMethod());
            DeleteIngredientCommand = new Command(arg => DeleteIngredientMethod(arg));

            SaveMeasurementCommand = new Command(arg => SaveMeasurementMethod());
            NewMeasurementCommand = new Command(arg => NewMeasurementMethod());
            DeleteMeasurementCommand = new Command(arg => DeleteMeasurementMethod(arg));
                                                    
            SaveBonusCardCommand = new Command(arg => SaveBonusCardMethod());
            NewBonusCardCommand = new Command(arg => NewBonusCardMethod());
            DeleteBonusCardCommand = new Command(arg => DeleteBonusCardMethod(arg));
            HistoryBonusCardCommand = new Command(arg => HistoryBonusCardMethod(arg));

            SaveTextInSellCommand = new Command(arg => SaveTextInSellMethod());
            NewTextInSellCommand = new Command(arg => NewTextInSellMethod());
            DeleteTextInSellCommand = new Command(arg => DeleteTextInSellMethod(arg));

            UpdateDishIngredientCommand = new Command((dd) => UpdateDishIngredientMethod(dd));

            AddIngredientToStoreCommand = new Command(arg => AddIngredientToStoreMethod());

            LoadTransactionCommand = new Command(arg=> LoadTransactionMethod());
            LoadTransactionCommonCommand = new Command(arg => LoadTransactionCommonMethod());
            
            StartTimeOfTransaction = DateTime.Now.AddDays(-1);
            EndTimeOfTransaction = DateTime.Now;

            StartTimeOfTransactionCommon = DateTime.Now.AddDays(-1);
            EndTimeOfTransactionCommon = DateTime.Now;
            LoadDishData();
        }
        #endregion

        #region Properties

        private ObservableCollection<User> _listUsers;
        public ObservableCollection<User> ListUsers
        {
            get { return _listUsers; }
            set
            {
                if (_listUsers != value)
                {
                    _listUsers = value;
                    OnPropertyChanged("ListUsers");
                }
            }
        }

        private ObservableCollection<Role> _listRoles;
        public ObservableCollection<Role> ListRoles
        {
            get { return _listRoles; }
            set
            {
                if (_listRoles != value)
                {
                    _listRoles = value;
                    OnPropertyChanged("ListRoles");
                }
            }
        }

        private User _selectedUser;
        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                if (_selectedUser != value)
                {
                    _selectedUser = value;
                    OnPropertyChanged("SelectedUser");
                }
            }
        }

        private ObservableCollection<TypeOfDish> _listTypeOfDish;
        public ObservableCollection<TypeOfDish> ListTypeOfDish
        {
            get { return _listTypeOfDish; }
            set
            {
                if (_listTypeOfDish != value)
                {
                    _listTypeOfDish = value;
                    OnPropertyChanged("ListTypeOfDish");
                }
            }
        }

        private ObservableCollection<Measurement> _listMeasurements;
        public ObservableCollection<Measurement> ListMeasurements
        {
            get { return _listMeasurements; }
            set
            {
                if (_listMeasurements != value)
                {
                    _listMeasurements = value;
                    OnPropertyChanged("ListMeasurements");
                }
            }
        }

        private ObservableCollection<BonusCard> _listBonusCards;
        public ObservableCollection<BonusCard> ListBonusCards
        {
            get { return _listBonusCards; }
            set
            {
                if (_listBonusCards != value)
                {
                    _listBonusCards = value;
                    OnPropertyChanged("ListBonusCards");
                }
            }
        }

        private ObservableCollection<TextInSell> _listTextInSells;
        public ObservableCollection<TextInSell> ListTextInSells
        {
            get { return _listTextInSells; }
            set
            {
                if (_listTextInSells != value)
                {
                    _listTextInSells = value;
                    OnPropertyChanged("ListTextInSells");
                }
            }
        }

        private ObservableCollection<Ingredient> _listIngredients;
        public ObservableCollection<Ingredient> ListIngredients
        {
            get { return _listIngredients; }
            set
            {
                if (_listIngredients != value)
                {
                    _listIngredients = value;
                    OnPropertyChanged("ListIngredients");
                }
            }
        }

        private ObservableCollection<DishInfo> _listDishesInfo;
        public ObservableCollection<DishInfo> ListDishesInfo
        {
            get { return _listDishesInfo; }
            set
            {
                if (_listDishesInfo != value)
                {
                    _listDishesInfo = value;
                    OnPropertyChanged("ListDishesInfo");
                }
            }
        }

        private DishInfo _selectedDishInfo;
        public DishInfo SelectedDishInfo
        {
            get { return _selectedDishInfo; }
            set
            {
                if (_selectedDishInfo != value)
                {
                    _selectedDishInfo = value;
                    OnPropertyChanged("SelectedDishInfo");
                }
            }
        }
        
        private Ingredient _selectedIngredient;
        public Ingredient SelectedIngredient
        {
            get { return _selectedIngredient; }
            set
            {
                if (_selectedIngredient != value)
                {
                    _selectedIngredient = value;
                    OnPropertyChanged("SelectedIngredient");
                }
            }
        }

        private Measurement _selectedMeasurement;
        public Measurement SelectedMeasurement
        {
            get { return _selectedMeasurement; }
            set
            {
                if (_selectedMeasurement != value)
                {
                    _selectedMeasurement = value;
                    OnPropertyChanged("SelectedMeasurement");
                }
            }
        }

        private BonusCard _selectedBonusCard;
        public BonusCard SelectedBonusCard
        {
            get { return _selectedBonusCard; }
            set
            {
                if (_selectedBonusCard != value)
                {
                    _selectedBonusCard = value;
                    OnPropertyChanged("SelectedBonusCard");
                }
            }
        }

        private TextInSell _selectedTextInSell;
        public TextInSell SelectedTextInSell
        {
            get { return _selectedTextInSell; }
            set
            {
                if (_selectedTextInSell != value)
                {
                    _selectedTextInSell = value;
                    OnPropertyChanged("SelectedTextInSell");
                }
            }
        }

        private Ingredient _addIngredientToStore;
        public Ingredient AddIngredientToStore
        {
            get { return _addIngredientToStore; }
            set
            {
                if (_addIngredientToStore != value)
                {
                    if (value != null)
                        _addIngredientToStore = new Ingredient() { Id = value.Id };
                    else
                        _addIngredientToStore = value;
                    OnPropertyChanged("AddIngredientToStore");
                }
            }
        }
        
        private ObservableCollection<TransactionInfo> _listOfTransaction;
        public ObservableCollection<TransactionInfo> ListOfTransaction
        {
            get { return _listOfTransaction; }
            set
            {
                if (_listOfTransaction != value)
                {
                    _listOfTransaction = value;
                    OnPropertyChanged("ListOfTransaction");
                }
            }
        }

        private DateTime _startTimeOfTransaction;
        public DateTime StartTimeOfTransaction
        {
            get { return _startTimeOfTransaction; }
            set
            {
                if (_startTimeOfTransaction != value)
                {
                    _startTimeOfTransaction = value;
                    OnPropertyChanged("StartTimeOfTransaction");
                }
            }
        }

        private DateTime _endTimeOfTransaction;
        public DateTime EndTimeOfTransaction
        {
            get { return _endTimeOfTransaction; }
            set
            {
                if (_endTimeOfTransaction != value)
                {
                    _endTimeOfTransaction = value;
                    OnPropertyChanged("EndTimeOfTransaction");
                }
            }
        }
        private double _AllSumTransaction;
        public double AllSumTransaction
        {
            get { return _AllSumTransaction; }
            set
            {
                if (_AllSumTransaction != value)
                {
                    _AllSumTransaction = value;
                    OnPropertyChanged("AllSumTransaction");
                }
            }
        }
        private bool _flagSell;
        public bool FlagSell
        {
            get { return _flagSell; }
            set
            {
                if (_flagSell != value)
                {
                    _flagSell = value;
                    OnPropertyChanged("FlagSell");
                }
            }
        }
        private bool _flagBuy;
        public bool FlagBuy
        {
            get { return _flagBuy; }
            set
            {
                if (_flagBuy != value)
                {
                    _flagBuy = value;
                    OnPropertyChanged("FlagBuy");
                }
            }
        }

        private ObservableCollection<TransactionInfo> _listOfTransactionCommon;
        public ObservableCollection<TransactionInfo> ListOfTransactionCommon
        {
            get { return _listOfTransactionCommon; }
            set
            {
                if (_listOfTransactionCommon != value)
                {
                    _listOfTransactionCommon = value;
                    OnPropertyChanged("ListOfTransactionCommon");
                }
            }
        }
        private double _AllSumTransactionCommon;
        public double AllSumTransactionCommon
        {
            get { return _AllSumTransactionCommon; }
            set
            {
                if (_AllSumTransactionCommon != value)
                {
                    _AllSumTransactionCommon = value;
                    OnPropertyChanged("AllSumTransactionCommon");
                }
            }
        }
        private bool _flagSellCommon;
        public bool FlagSellCommon
        {
            get { return _flagSellCommon; }
            set
            {
                if (_flagSellCommon != value)
                {
                    _flagSellCommon = value;
                    OnPropertyChanged("FlagSellCommon");
                }
            }
        }
        private bool _flagBuyCommon;
        public bool FlagBuyCommon
        {
            get { return _flagBuyCommon; }
            set
            {
                if (_flagBuyCommon != value)
                {
                    _flagBuyCommon = value;
                    OnPropertyChanged("FlagBuyCommon");
                }
            }
        }

        private DateTime _startTimeOfTransactionCommon;
        public DateTime StartTimeOfTransactionCommon
        {
            get { return _startTimeOfTransactionCommon; }
            set
            {
                if (_startTimeOfTransactionCommon != value)
                {
                    _startTimeOfTransactionCommon = value;
                    OnPropertyChanged("StartTimeOfTransactionCommon");
                }
            }
        }

        private DateTime _endTimeOfTransactionCommon;
        public DateTime EndTimeOfTransactionCommon
        {
            get { return _endTimeOfTransactionCommon; }
            set
            {
                if (_endTimeOfTransactionCommon != value)
                {
                    _endTimeOfTransactionCommon = value;
                    OnPropertyChanged("EndTimeOfTransactionCommon");
                }
            }
        }
        #endregion

        #region Commands
        public ICommand SaveUserCommand { get; set; }
        public ICommand NewUserCommand { get; set; }
        public ICommand DeleteUserCommand { get; set; }
        
        public ICommand SaveDishCommand { get; set; }
        public ICommand NewDishCommand { get; set; }
        public ICommand DeleteDishCommand { get; set; }

        public ICommand SaveIngredientCommand { get; set; }
        public ICommand NewIngredientCommand { get; set; }
        public ICommand DeleteIngredientCommand { get; set; }

        public ICommand SaveMeasurementCommand { get; set; }
        public ICommand NewMeasurementCommand { get; set; }
        public ICommand DeleteMeasurementCommand { get; set; }

        public ICommand SaveBonusCardCommand { get; set; }
        public ICommand NewBonusCardCommand { get; set; }
        public ICommand DeleteBonusCardCommand { get; set; }
        public ICommand HistoryBonusCardCommand { get; set; }
        
        public ICommand SaveTextInSellCommand { get; set; }
        public ICommand NewTextInSellCommand { get; set; }
        public ICommand DeleteTextInSellCommand { get; set; }

        public ICommand AddIngredientToStoreCommand { get; set; }
        public ICommand UpdateDishIngredientCommand { get; set; }

        public ICommand LoadTransactionCommand { get; set; }
        public ICommand LoadTransactionCommonCommand { get; set; }
        #endregion

        #region Methods
        void LoadUserData()
        {
            using (var db = new BurDBEntities())
            {
                ListUsers = new ObservableCollection<User>(db.Users);
                ListRoles = new ObservableCollection<Role>(db.Roles);
                foreach (User i_user in ListUsers)
                    i_user.Role = ListRoles.FirstOrDefault(r => r.Id == i_user.IdRole);
            }
        }
        void SaveUserMethod(object Password)
        {
            if(ListUsers.Count(u => u.Login== SelectedUser.Login) > 1)
                MessageBox.Show("Пользователь с таким логином уже существует!");
            else if(!ListUsers.Contains(SelectedUser))
                MessageBox.Show("Пользователь не найден!\nДля изменения выберите из списка.");
            else if (SelectedUser.FullName != "" && SelectedUser.Login != "" && SelectedUser.Role != null)
            {
                using (var db = new BurDBEntities())
                {
                    var n_user = db.Users.FirstOrDefault(u => u.Id == SelectedUser.Id);
                    n_user.FullName = SelectedUser.FullName;
                    n_user.Login = SelectedUser.Login;
                    n_user.IdRole = SelectedUser.Role.Id;
                    if(((System.Windows.Controls.PasswordBox)Password).Password != "")
                        n_user.Password = ((System.Windows.Controls.PasswordBox)Password).Password;
                    db.SaveChanges();
                }
                SelectedUser = new User();
                ((System.Windows.Controls.PasswordBox)Password).Password = "";
                MessageBox.Show("Пользователь сохранен!");
            }
            else MessageBox.Show("Заполните все поля!");
        }
        void NewUserMethod(object Password)
        {
            if (ListUsers.Count(u => u.Login == SelectedUser.Login) > 1)
                MessageBox.Show("Пользователь с таким логином уже существует!");
            else if (SelectedUser.FullName != "" && SelectedUser.Login != "" && SelectedUser.Role != null && ((System.Windows.Controls.PasswordBox)Password).Password != "")
            {
                using (var db = new BurDBEntities())
                {
                    var n_user = new User();
                    n_user.FullName = SelectedUser.FullName;
                    n_user.Login = SelectedUser.Login;
                    n_user.IdRole = SelectedUser.Role.Id;
                    n_user.Password = ((System.Windows.Controls.PasswordBox)Password).Password;
                    db.Users.Add(n_user);
                    db.SaveChanges();
                    LoadUserData();
                }
                SelectedUser = new User();
                ((System.Windows.Controls.PasswordBox)Password).Password = "";
                MessageBox.Show("Пользователь сохранен в базу!");
            }
            else MessageBox.Show("Заполните все поля!");
        }
        void DeleteUserMethod(object id)
        {
            SelectedUser = ListUsers.FirstOrDefault(u => u.Id == (int)id);
            using (var db = new BurDBEntities())
            {
                if (ListUsers.Where(u => u.Role.Name == "admin").Count() > 1)
                {
                    foreach (Transaction tr in db.Transactions.Where(t => t.IdUser == SelectedUser.Id))
                        tr.IdUser = null;
                    db.Users.Remove(db.Users.FirstOrDefault(u => u.Id == SelectedUser.Id));
                    db.SaveChanges();
                    ListUsers.Remove(SelectedUser);
                    SelectedUser = new User();
                    MessageBox.Show("Пользователь удален из базы!");
                }
                else
                    MessageBox.Show("Невозможно удалить всех администраторов!");
            }
        }

        void LoadDishData()
        {
            using (var db = new BurDBEntities())
            {
                ListTypeOfDish = new ObservableCollection<TypeOfDish>(db.TypeOfDishes);
                ListMeasurements = new ObservableCollection<Measurement>(db.Measurements.OrderBy(m => m.Name));
                ListBonusCards = new ObservableCollection<BonusCard>(db.BonusCards);
                ListTextInSells = new ObservableCollection<TextInSell>(db.TextInSells);
                ListIngredients = new ObservableCollection<Ingredient>(db.Ingredients.OrderBy(i => i.Name));
                ListDishesInfo = new ObservableCollection<DishInfo>();
                foreach (Ingredient i_ingredient in ListIngredients)
                    i_ingredient.Measurement = ListMeasurements.FirstOrDefault(m => m.Id == i_ingredient.IdMeasurement);
                foreach (Dish i_dish in db.Dishes)
                {
                    i_dish.TypeOfDish = ListTypeOfDish.FirstOrDefault(t => t.Id == i_dish.IdType);
                    string ingredientList = "";
                    foreach (DishIngredientList i_di in db.DishIngredientLists.Where(di => di.IdDish == i_dish.Id))
                        ingredientList += string.Format("\n{0,-20} {1}", i_di.Ingredient.Name, i_di.Amount + i_di.Ingredient.Measurement.Name);
                    if (ingredientList != "")
                        ingredientList = ingredientList.Remove(0, 1);
                    ListDishesInfo.Add(new DishInfo(i_dish, ingredientList));
                }
                ListDishesInfo = new ObservableCollection<DishInfo>(ListDishesInfo.OrderBy(d => d.Dish.Name));
            }
        }

        void UpdateDishIngredientMethod(object dish)
        {
            ViewShower.ShowIngredientOfDishView(ListDishesInfo.FirstOrDefault(d => d.Dish.Id == ((Dish)dish).Id));
            string ingredientList = "";
            using (var db = new BurDBEntities())
            {
                foreach (DishIngredientList i_di in db.DishIngredientLists.Where(di => di.IdDish == ((Dish)dish).Id))
                    ingredientList += string.Format("\n{0,-20} {1}", i_di.Ingredient.Name, i_di.Amount + i_di.Ingredient.Measurement.Name);
                if (ingredientList != "")
                    ingredientList = ingredientList.Remove(0, 1);
            }
            ListDishesInfo.FirstOrDefault(d => d.Dish.Id == ((Dish)dish).Id).IngredientList = ingredientList;
        }

        void SaveDishMethod()
        {
            using (var db = new BurDBEntities())
            {
                Dish tmpDish;
                foreach (Dish i_d in db.Dishes)
                {
                    tmpDish = ListDishesInfo.FirstOrDefault(d => d.Dish.Id == i_d.Id).Dish;
                    i_d.Name = tmpDish.Name;
                    i_d.IdType = tmpDish.TypeOfDish.Id;
                    i_d.Price = tmpDish.Price;
                }
                db.SaveChanges();
            }
            MessageBox.Show("Сохранено!");
        }
        void NewDishMethod()
        {
            SelectedDishInfo = new DishInfo(new Dish(), "");
            SelectedDishInfo.Dish.Name = "<новое блюдо>";
            SelectedDishInfo.Dish.Price = 0;
            SelectedDishInfo.Dish.IdType = ListTypeOfDish.FirstOrDefault().Id;
            using (var db = new BurDBEntities())
            {
                db.Dishes.Add(SelectedDishInfo.Dish);
                db.SaveChanges();
            }
            SelectedDishInfo.Dish.TypeOfDish = ListTypeOfDish.FirstOrDefault();
            ListDishesInfo.Add(SelectedDishInfo);
            ListDishesInfo = new ObservableCollection<DishInfo>(ListDishesInfo.OrderBy(d => d.Dish.Name));
            MessageBox.Show("Блюдо сохранено в базе! Измените поля и нажмите \"Сохранить изменения\" для изменения в базе.");
        }
        void DeleteDishMethod(object dish)
        {
            MessageBoxResult result = MessageBox.Show("Вы уверены?", "Confirmation", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                SelectedDishInfo = ListDishesInfo.FirstOrDefault(d => d.Dish.Id == ((Dish)dish).Id);
                if (SelectedDishInfo != null)
                {
                    using (var db = new BurDBEntities())
                    {
                        foreach (DishIngredientList i_di in db.DishIngredientLists.Where(di => di.IdDish == SelectedDishInfo.Dish.Id))
                            db.DishIngredientLists.Remove(i_di);
                        db.Dishes.Remove(db.Dishes.FirstOrDefault(d => d.Id == SelectedDishInfo.Dish.Id));
                        db.SaveChanges();
                    }
                    ListDishesInfo.Remove(SelectedDishInfo);
                    SelectedDishInfo = null;
                    MessageBox.Show("Блюдо удалено из базы!");
                }
            } 
        }

        void SaveIngredientMethod()
        {
            using (var db = new BurDBEntities())
            {
                Ingredient tmpIngredient;
                foreach (Ingredient i_d in db.Ingredients)
                {
                    tmpIngredient = ListIngredients.FirstOrDefault(i => i.Id == i_d.Id);
                    i_d.Name = tmpIngredient.Name;
                    i_d.IdMeasurement = tmpIngredient.Measurement.Id;
                }
                db.SaveChanges();
            }
            MessageBox.Show("Сохранено!");
        }
        void NewIngredientMethod()
        {
            SelectedIngredient = new Ingredient();
            SelectedIngredient.Name = "<новый продукт>";
            SelectedIngredient.Amount = 0;
            SelectedIngredient.IdMeasurement = ListMeasurements.FirstOrDefault().Id;
            using (var db = new BurDBEntities())
            {
                db.Ingredients.Add(SelectedIngredient);
                db.SaveChanges();
            }
            SelectedIngredient.Measurement = ListMeasurements.FirstOrDefault();
            ListIngredients.Add(SelectedIngredient);
            ListIngredients = new ObservableCollection<Ingredient>(ListIngredients.OrderBy(i => i.Name));
            MessageBox.Show("Продукт сохранен в базе! Измените поля и нажмите \"Сохранить изменения\" для изменения в базе.");
        }
        void DeleteIngredientMethod(object id)
        {
            SelectedIngredient = ListIngredients.FirstOrDefault(i => i.Id == (int)id);
            if (SelectedIngredient != null)
            {
                using (var db = new BurDBEntities())
                {
                    foreach (DishIngredientList i_di in db.DishIngredientLists.Where(di => di.IdIngredient == SelectedIngredient.Id))
                        db.DishIngredientLists.Remove(i_di);
                    db.Ingredients.Remove(db.Ingredients.FirstOrDefault(i => i.Id == SelectedIngredient.Id));
                    db.SaveChanges();
                }
                ListIngredients.Remove(SelectedIngredient);
                SelectedIngredient = null;
                MessageBox.Show("Продукт удален из базы!");
            }
        }
        
        void SaveMeasurementMethod()
        {
            using (var db = new BurDBEntities())
            {
                Measurement tmpMeasurement;
                foreach (Measurement i_m in db.Measurements)
                {
                    tmpMeasurement = ListMeasurements.FirstOrDefault(m => m.Id == i_m.Id);
                    i_m.Name = tmpMeasurement.Name;
                }
                db.SaveChanges();
            }
            MessageBox.Show("Сохранено!");
        }
        void NewMeasurementMethod()
        {
            SelectedMeasurement = new Measurement();
            SelectedMeasurement.Name = "<новое измерение продукта>";
            using (var db = new BurDBEntities())
            {
                db.Measurements.Add(SelectedMeasurement);
                db.SaveChanges();
            }
            ListMeasurements.Add(SelectedMeasurement);
            MessageBox.Show("Мера сохранена в базе! Измените поля и нажмите \"Сохранить изменения\" для изменения в базе.");
        }
        void DeleteMeasurementMethod(object id)
        {
            SelectedMeasurement = ListMeasurements.FirstOrDefault(m => m.Id == (int)id);
            if (SelectedMeasurement != null && SelectedMeasurement.Id != 1)
            {
                foreach (Ingredient i_i in ListIngredients.Where(i => i.Measurement.Id == SelectedMeasurement.Id))
                    i_i.Measurement = ListMeasurements.FirstOrDefault(m => m.Id == 1);
                using (var db = new BurDBEntities())
                {
                    foreach (Ingredient i_i in db.Ingredients.Where(i => i.IdMeasurement == SelectedMeasurement.Id))
                        i_i.IdMeasurement = 1;
                    db.Measurements.Remove(db.Measurements.FirstOrDefault(m => m.Id == SelectedMeasurement.Id));
                    db.SaveChanges();
                }
                ListMeasurements.Remove(SelectedMeasurement);
                SelectedMeasurement = null;
                MessageBox.Show("Мера удалена из базы!");
            }
        }

        void SaveBonusCardMethod()
        {
            using (var db = new BurDBEntities())
            {
                BonusCard tmpBonusCard;
                foreach (BonusCard i_card in db.BonusCards)
                {
                    tmpBonusCard = ListBonusCards.FirstOrDefault(m => m.Id == i_card.Id);
                    i_card.Name = tmpBonusCard.Name;
                    i_card.Card_number = tmpBonusCard.Card_number;
                    i_card.Bonus = tmpBonusCard.Bonus;
                }
                db.SaveChanges();
            }
            MessageBox.Show("Сохранено!");
        }
        void NewBonusCardMethod()
        {
            SelectedBonusCard = new BonusCard();
            SelectedBonusCard.Name = "<name>";
            SelectedBonusCard.Card_number = "<number>";
            SelectedBonusCard.Bonus = 0;
            using (var db = new BurDBEntities())
            {
                db.BonusCards.Add(SelectedBonusCard);
                db.SaveChanges();
            }
            ListBonusCards.Add(SelectedBonusCard);
            MessageBox.Show("Карта сохранена в базе! Измените поля и нажмите \"Сохранить изменения\" для изменения в базе.");
        }
        void DeleteBonusCardMethod(object id)
        {
            SelectedBonusCard = ListBonusCards.FirstOrDefault(m => m.Id == (int)id);
            if (SelectedBonusCard != null)
            {
                using (var db = new BurDBEntities())
                {
                    db.BonusCards.Remove(db.BonusCards.FirstOrDefault(m => m.Id == SelectedBonusCard.Id));
                    db.SaveChanges();
                }
                ListBonusCards.Remove(SelectedBonusCard);
                SelectedBonusCard = null;
                MessageBox.Show("Карта удалена из базы!");
            }
        }

        void HistoryBonusCardMethod(object id)
        {
            ViewShower.ShowHistoryBonusCardView(ListBonusCards.FirstOrDefault(d => d.Id == (int)id));
        }

        void SaveTextInSellMethod()
        {
            using (var db = new BurDBEntities())
            {
                TextInSell tmpTextInSell;
                foreach (TextInSell i_text in db.TextInSells)
                {
                    tmpTextInSell = ListTextInSells.FirstOrDefault(m => m.Id == i_text.Id);
                    i_text.Text = tmpTextInSell.Text;
                    i_text.Flag = tmpTextInSell.Flag;
                }
                db.SaveChanges();
            }
            MessageBox.Show("Сохранено!");
        }
        void NewTextInSellMethod()
        {
            SelectedTextInSell = new TextInSell();
            SelectedTextInSell.Text = "<text>";
            SelectedTextInSell.Flag = 1;
            using (var db = new BurDBEntities())
            {
                db.TextInSells.Add(SelectedTextInSell);
                db.SaveChanges();
            }
            ListTextInSells.Add(SelectedTextInSell);
            MessageBox.Show("Пожелание сохранено в базе! Измените поля и нажмите \"Сохранить изменения\" для изменения в базе.");
        }
        void DeleteTextInSellMethod(object id)
        {
            SelectedTextInSell = ListTextInSells.FirstOrDefault(m => m.Id == (int)id);
            if (SelectedTextInSell != null)
            {
                using (var db = new BurDBEntities())
                {
                    db.TextInSells.Remove(db.TextInSells.FirstOrDefault(m => m.Id == SelectedTextInSell.Id));
                    db.SaveChanges();
                }
                ListTextInSells.Remove(SelectedTextInSell);
                SelectedTextInSell = null;
                MessageBox.Show("Пожелание удалено из базы!");
            }
        }

        void AddIngredientToStoreMethod()
        {
            if (AddIngredientToStore != null && AddIngredientToStore.Amount != null)
            {
                using (var db = new BurDBEntities())
                {
                    string name = db.Ingredients.FirstOrDefault(i => i.Id == AddIngredientToStore.Id).Name;
                    var tran = new Transaction()
                    {
                        Name = name,
                        Date = DateTime.Now,
                        Amount = AddIngredientToStore.Amount,
                        IdType = 2,
                        Price = 0,
                        IdUser = user.Id
                    };
                    db.Transactions.Add(tran);
                    db.Ingredients.FirstOrDefault(i => i.Id == AddIngredientToStore.Id).Amount += AddIngredientToStore.Amount;
                    db.SaveChanges();
                }
                ListIngredients.FirstOrDefault(i => i.Id == AddIngredientToStore.Id).Amount += AddIngredientToStore.Amount;
                ListIngredients =  new ObservableCollection<Ingredient>(ListIngredients.OrderBy(i => i.Name));
                AddIngredientToStore = null;
            }
        }

        void LoadTransactionMethod()
        {
            if (StartTimeOfTransaction != null && EndTimeOfTransaction != null)
                using (var db = new BurDBEntities())
                {
                    string nameUser, bonusStr;
                    AllSumTransaction = 0;
                    ListOfTransaction = new ObservableCollection<TransactionInfo>();
                    int t1 = 0;
                    int t2 = 0;
                    if (FlagSell) t1 = db.TypeOfTransactions.FirstOrDefault(tt => tt.Name == "sell").Id;
                    if (FlagBuy) t2 = db.TypeOfTransactions.FirstOrDefault(tt => tt.Name == "buy").Id;
                    foreach (var tran in db.Transactions.Where(t => t.Date > StartTimeOfTransaction && t.Date < EndTimeOfTransaction && (t.IdType==t1 || t.IdType ==t2)).OrderBy(t => t.Date).ToList())
                    {
                        if (tran.IdUser == null) nameUser = "<удален>"; else nameUser = tran.User.FullName;
                        if (tran.BonusCardNumbet != null) bonusStr = tran.BonusCardNumbet + "-" + tran.Bonus; else bonusStr = "";
                        ListOfTransaction.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), tran.TypeOfTransaction.Name, nameUser, tran.Date.Value.ToString("MM-dd-yyyy HH:mm"), bonusStr));
                        AllSumTransaction += (tran.Price ?? 0) * (tran.Amount ?? 0);
                    }
                }
        }
        void LoadTransactionCommonMethod()
        {
            if (StartTimeOfTransactionCommon != null && EndTimeOfTransactionCommon != null)
                using (var db = new BurDBEntities())
                {
                    AllSumTransactionCommon = 0;
                    ListOfTransactionCommon = new ObservableCollection<TransactionInfo>();
                    TransactionInfo tmp;
                    int t1 = 0;
                    int t2 = 0;
                    if (FlagSellCommon) t1 = db.TypeOfTransactions.FirstOrDefault(tt => tt.Name == "sell").Id;
                    if (FlagBuyCommon) t2 = db.TypeOfTransactions.FirstOrDefault(tt => tt.Name == "buy").Id;
                    foreach (var tran in db.Transactions.Where(t => t.Date > StartTimeOfTransactionCommon && t.Date < EndTimeOfTransactionCommon && (t.IdType == t1 || t.IdType == t2)).OrderBy(t => t.Name))
                    {
                        AllSumTransactionCommon += (tran.Price ?? 0) * (tran.Amount ?? 0);
                        tmp = ListOfTransactionCommon.FirstOrDefault(i_t => i_t.Name == tran.Name && i_t.Type == tran.TypeOfTransaction.Name && i_t.Price == tran.Price.ToString());
                        if (tmp == null)
                            ListOfTransactionCommon.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), tran.TypeOfTransaction.Name, "", "", ""));
                        else
                            tmp.Amount = (Convert.ToDouble(tmp.Amount) + tran.Amount ?? 0).ToString();
                    }
                }
        }
        #endregion
    }
}
