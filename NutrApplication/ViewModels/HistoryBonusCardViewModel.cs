﻿using NutrApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NutrApplication.ViewModels
{
    class HistoryBonusCardViewModel
    {
        public HistoryBonusCardViewModel(BonusCard card)
        {
            using (var db = new BurDBEntities())
            {
                string nameUser, bonusStr;
                AllSumTransaction = 0;
                ListOfTransaction = new ObservableCollection<TransactionInfo>();
                foreach (var tran in db.Transactions.Where(t => t.BonusCardNumbet == card.Card_number).OrderBy(t => t.Date).ToList())
                {
                    if (tran.IdUser == null) nameUser = "<удален>"; else nameUser = tran.User.FullName;
                    if (tran.BonusCardNumbet != null) bonusStr = tran.BonusCardNumbet + "-" + tran.Bonus; else bonusStr = "";
                    ListOfTransaction.Add(new TransactionInfo(tran.Name, tran.Price.ToString(), tran.Amount.ToString(), tran.TypeOfTransaction.Name, nameUser, tran.Date.Value.ToString("MM-dd-yyyy HH:mm"), bonusStr));
                    AllSumTransaction += (tran.Price ?? 0) * (tran.Amount ?? 0);
                }
            }
        }

        public ObservableCollection<TransactionInfo> ListOfTransaction
        {
            get;
            set;
        }
        public double AllSumTransaction
        {
            get;
            set;
        }

    }

}
