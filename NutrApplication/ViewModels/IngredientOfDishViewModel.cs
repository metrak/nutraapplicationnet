﻿using NutrApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace NutrApplication.ViewModels
{
    class IngredientOfDishViewModel : BaseViewModel, INotifyPropertyChanged
    {

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Constructor
        public IngredientOfDishViewModel(DishInfo dish)
        {
            SelectedDish = dish;

            SelectedDishIngredient = new DishIngredientList();
            LoadIngredientData();
            SaveDishIngredientCommand = new Command(arg => SaveDishIngredientMethod());
            NewDishIngredientCommand = new Command(arg => NewDishIngredientMethod());
            DeleteDishIngredientCommand = new Command(arg => DeleteDishIngredientMethod(arg));
            //UpdateIngredientCommand = new Command(arg => UpdateIngredientMethod());

            //LoadDishData();
        }
        #endregion

        #region Properties

        private ObservableCollection<DishIngredientList> _listOfDishIngredients;
        public ObservableCollection<DishIngredientList> ListOfDishIngredients
        {
            get { return _listOfDishIngredients; }
            set
            {
                if (_listOfDishIngredients != value)
                {
                    _listOfDishIngredients = value;
                    OnPropertyChanged("ListOfDishIngredients");
                }
            }
        }

        private ObservableCollection<Ingredient> _listIngredients;
        public ObservableCollection<Ingredient> ListIngredients
        {
            get { return _listIngredients; }
            set
            {
                if (_listIngredients != value)
                {
                    _listIngredients = value;
                    OnPropertyChanged("ListIngredients");
                }
            }
        }

        private DishInfo _selectedDish;
        public DishInfo SelectedDish
        {
            get { return _selectedDish; }
            set
            {
                if (_selectedDish != value)
                {
                    _selectedDish = value;
                    OnPropertyChanged("SelectedDish");
                }
            }
        }

        private DishIngredientList _selectedDishIngredient;
        public DishIngredientList SelectedDishIngredient
        {
            get { return _selectedDishIngredient; }
            set
            {
                if (_selectedDishIngredient != value)
                {
                    _selectedDishIngredient = value;
                    OnPropertyChanged("SelectedDishIngredient");
                }
            }
        }

        #endregion

        #region Commands
        public ICommand SaveDishIngredientCommand { get; set; }
        public ICommand NewDishIngredientCommand { get; set; }
        public ICommand DeleteDishIngredientCommand { get; set; }
        
        #endregion

        #region Methods
        void LoadIngredientData()
        {
            using (var db = new BurDBEntities())
            {
                ListOfDishIngredients = new ObservableCollection<DishIngredientList>(db.DishIngredientLists.Where(di => di.IdDish == SelectedDish.Dish.Id));
                ListIngredients = new ObservableCollection<Ingredient>(db.Ingredients.OrderBy(i => i.Name));
                foreach (Ingredient i_ingredient in ListIngredients)
                    i_ingredient.Measurement = db.Measurements.FirstOrDefault(m => m.Id == i_ingredient.IdMeasurement);
                foreach (DishIngredientList i_dishIngredientList in ListOfDishIngredients)
                    i_dishIngredientList.Ingredient = ListIngredients.FirstOrDefault(i => i.Id == i_dishIngredientList.IdIngredient);
            }
        }

        void SaveDishIngredientMethod()
        {
            using (var db = new BurDBEntities())
            {
                DishIngredientList tmpDishIngredientList;
                foreach (DishIngredientList i_di in db.DishIngredientLists.Where(di => di.IdDish == SelectedDish.Dish.Id))
                {
                    tmpDishIngredientList = ListOfDishIngredients.FirstOrDefault(di => di.Id == i_di.Id);
                    i_di.IdIngredient = tmpDishIngredientList.Ingredient.Id;
                    i_di.Amount = tmpDishIngredientList.Amount;
                }
                db.SaveChanges();
            }
            MessageBox.Show("Сохранено!");
            //Application.Current.MainWindow.Close();
        }
        void NewDishIngredientMethod()
        {
            SelectedDishIngredient = new DishIngredientList()
            {
                IdIngredient = ListIngredients.FirstOrDefault().Id,
                Amount = 0,
                IdDish = SelectedDish.Dish.Id
            };
            using (var db = new BurDBEntities())
            {
                db.DishIngredientLists.Add(SelectedDishIngredient);
                db.SaveChanges();
            }
            SelectedDishIngredient.Ingredient = ListIngredients.FirstOrDefault();
            ListOfDishIngredients.Add(SelectedDishIngredient);
            MessageBox.Show("Ингредиент сохранен в базе! Измените поля и нажмите \"Сохранить изменения\" для изменения в базе.");
        }
        void DeleteDishIngredientMethod(object id)
        {
            SelectedDishIngredient = ListOfDishIngredients.FirstOrDefault(di => di.Id == (int)id);
            if (SelectedDishIngredient != null)
            {
                using (var db = new BurDBEntities())
                {
                    db.DishIngredientLists.Remove(db.DishIngredientLists.FirstOrDefault(di => di.Id == SelectedDishIngredient.Id));
                    db.SaveChanges();
                }
                ListOfDishIngredients.Remove(SelectedDishIngredient);
                SelectedDishIngredient = null;
                MessageBox.Show("Ингредиент удален из базы!");
            }
        }
        #endregion
    }
}
