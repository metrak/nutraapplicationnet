﻿using NutrApplication.Models;
using NutrApplication.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NutrApplication.ViewModels
{
    class ViewShower
    {
        private static Window wnd;
        public static void Show(User user)
        {
            wnd = null;
            switch (user.Role.Name)
            {
                case "user":
                    wnd = new UserView() { DataContext = new UserViewModel() { user = user } };
                    break;
                case "admin":
                    wnd = new AdminView() { DataContext = new AdminViewModel() { user = user } }; ;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("p_viewIndex", "Такого индекса View не существует");
            }
            if (wnd != null)
            {
                wnd.Show();
                Application.Current.MainWindow.Close();
               
            }
        }
        public static void ShowIngredientOfDishView(DishInfo dish)
        {
            Window wndIngredient = null;
            wndIngredient = new IngredientOfDishView() { DataContext = new IngredientOfDishViewModel(dish) };
            if (wndIngredient != null)
            {
                wndIngredient.ShowDialog();
            }
        }
        public static void ShowHistoryBonusCardView(BonusCard card)
        {
            Window wndTransaction = null;
            wndTransaction = new HistoryBonusCardView() { DataContext = new HistoryBonusCardViewModel(card) };
            if (wndTransaction != null)
            {
                wndTransaction.ShowDialog();
            }
        }
        public static void Exit()
        {
            var mw = new MainWindow
            {
                DataContext = new MainWindowViewModel()
            };
            mw.Show();
            wnd.Close();
        }
    }
}
