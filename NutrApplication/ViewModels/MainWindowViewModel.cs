﻿using NutrApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace NutrApplication.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
  
        public MainWindowViewModel()
        {
            EnterCommand = new Command((pass) => EnterMethod(pass));
        }
        
        private string _login;
        private string _message;
        public string Login
        {
            get { return _login; }
            set
            {
                if (_login != value)
                {
                    _login = value;
                    OnPropertyChanged("Login");
                }
            }
        }
        public string Message
        {
            get { return _message; }
            set
            {
                if (_message != value)
                {
                    _message = value;
                    OnPropertyChanged("Message");
                }
            }
        }
        public ICommand EnterCommand { get; set; }

        private void EnterMethod(object Password)
        {
           // try
            //{
                using (var db = new BurDBEntities())
                {

                //db.Database.Connection.Open();
                var us = db.Users.Where(u => u.Login == Login && u.Password == ((System.Windows.Controls.PasswordBox)Password).Password).ToList();
                    if (us.Capacity != 0)
                        ViewShower.Show(us.First());
                    else
                        Message = "Неверный логин или пароль";
                }
                Login = "";
                ((System.Windows.Controls.PasswordBox)Password).Password = "";
            //}
            //catch { MessageBox.Show("Error Establishing a Database Connection"); }
        }
    }
}
